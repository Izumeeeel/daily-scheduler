
importScripts(
  'https://www.gstatic.com/firebasejs/7.16.0/firebase-app.js'
)
importScripts(
  'https://www.gstatic.com/firebasejs/7.16.0/firebase-messaging.js'
)
firebase.initializeApp({"apiKey":"AIzaSyCzIdCoZInxoJwBEyK84L6ChRZRJKo5K_g","authDomain":"meta-aura-219108.firebaseapp.com","databaseURL":"https:\u002F\u002Fmeta-aura-219108.firebaseio.com","projectId":"meta-aura-219108","storageBucket":"meta-aura-219108.appspot.com","messagingSenderId":"273659631095","appId":"1:273659631095:web:5ead310a1cd56e29a14af2","measurementId":"G-4TXZ2CM2MQ"})

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging()

// Setup event listeners for actions provided in the config:
self.addEventListener('notificationclick', function(e) {
  const actions = [{"action":"goToUrl","url":"https:\u002F\u002Fgithub.com\u002Flupas"}]
  const action = actions.find(x => x.id === e.action.action)
  const notification = e.notification

  if (!action) return

  if (action.url) {
    clients.openWindow(action.url)
    notification.close()
  }
})
